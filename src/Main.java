import Lexer.*;
import Parcer.*;
import javafx.scene.Parent;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        Lexer lexer = new Lexer();
        LinkedList<Token> tokens = lexer.lex("a = 1; while (a < 2){ ZAnoza = a; a--;}");

        for (int i = 0; i < tokens.size(); i++)
        {
            System.out.println(tokens.get(i));
        }
        try {
            Parser.parse(tokens);
        }catch ( Exception ex)
        {
            System.err.println(ex);
            System.exit(1);
        }
    }
}
